﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Stingray Developer Console")]
[assembly: AssemblyDescription("Mar 2017 - August 2017")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Endava    http://www.endava.com")]
[assembly: AssemblyProduct("Stingray Developer Console")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("Dan Dragut <dan.dragut@endava.com>")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("34acad40-652f-49a9-a1e0-5538388b788b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.2017.08.02")]
[assembly: AssemblyFileVersion("1.2017.08.02")]
