﻿namespace Stingray_Developer_Console
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSourcesLocation = new System.Windows.Forms.TextBox();
            this.buttonBrowseSourcesLocation = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageDictionaries = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxLabelFilter = new System.Windows.Forms.TextBox();
            this.buttonDictionariesFilter = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxIdFilter = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxProductFilter = new System.Windows.Forms.TextBox();
            this.splitContainerDictionaries = new System.Windows.Forms.SplitContainer();
            this.dataGridViewDictionaries = new System.Windows.Forms.DataGridView();
            this.textBoxDictionaryItem = new System.Windows.Forms.TextBox();
            this.tabPageActions = new System.Windows.Forms.TabPage();
            this.buttonActionsClearFilter = new System.Windows.Forms.Button();
            this.buttonActionsFilter = new System.Windows.Forms.Button();
            this.dataTreeListViewActions = new BrightIdeasSoftware.DataTreeListView();
            this.olvColumnName = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumnClass = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.contextMenuStripActions = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemActionsCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxActionNameFilter = new System.Windows.Forms.TextBox();
            this.tabPageEvents = new System.Windows.Forms.TabPage();
            this.buttonEventsClearFilter = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxEventLabelFilter = new System.Windows.Forms.TextBox();
            this.buttonEventsFilter = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxEventActionFilter = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxEventNameFilter = new System.Windows.Forms.TextBox();
            this.splitContainerEvents = new System.Windows.Forms.SplitContainer();
            this.dataGridViewEvents = new System.Windows.Forms.DataGridView();
            this.tabControlEvent = new System.Windows.Forms.TabControl();
            this.tabPageEventDetails = new System.Windows.Forms.TabPage();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxEventProductCheck = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxEventAgencyCheck = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxEventMatchStatus = new System.Windows.Forms.TextBox();
            this.buttonEventActionGoTo = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxEventACL = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxEventPath = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxEventStrategy = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxEventLabel = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxEventAction = new System.Windows.Forms.TextBox();
            this.textBoxEventName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPageEventXML = new System.Windows.Forms.TabPage();
            this.textBoxEventXML = new System.Windows.Forms.TextBox();
            this.tabPageDocuments = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dataGridViewDocuments = new System.Windows.Forms.DataGridView();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabPageStatistics = new System.Windows.Forms.TabPage();
            this.dataGridViewStatistics = new System.Windows.Forms.DataGridView();
            this.imageListTabs = new System.Windows.Forms.ImageList(this.components);
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripTimingLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttonParseSources = new System.Windows.Forms.Button();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.menuStripItemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.labelEventMatchStatus = new System.Windows.Forms.Label();
            this.labelEventAgencyCheck = new System.Windows.Forms.Label();
            this.labelEventProductCheck = new System.Windows.Forms.Label();
            this.tabControlMain.SuspendLayout();
            this.tabPageDictionaries.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerDictionaries)).BeginInit();
            this.splitContainerDictionaries.Panel1.SuspendLayout();
            this.splitContainerDictionaries.Panel2.SuspendLayout();
            this.splitContainerDictionaries.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDictionaries)).BeginInit();
            this.tabPageActions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataTreeListViewActions)).BeginInit();
            this.contextMenuStripActions.SuspendLayout();
            this.tabPageEvents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerEvents)).BeginInit();
            this.splitContainerEvents.Panel1.SuspendLayout();
            this.splitContainerEvents.Panel2.SuspendLayout();
            this.splitContainerEvents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEvents)).BeginInit();
            this.tabControlEvent.SuspendLayout();
            this.tabPageEventDetails.SuspendLayout();
            this.tabPageEventXML.SuspendLayout();
            this.tabPageDocuments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDocuments)).BeginInit();
            this.tabPageStatistics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStatistics)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.menuStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sources location:";
            // 
            // textBoxSourcesLocation
            // 
            this.textBoxSourcesLocation.Location = new System.Drawing.Point(104, 30);
            this.textBoxSourcesLocation.Name = "textBoxSourcesLocation";
            this.textBoxSourcesLocation.Size = new System.Drawing.Size(743, 21);
            this.textBoxSourcesLocation.TabIndex = 1;
            // 
            // buttonBrowseSourcesLocation
            // 
            this.buttonBrowseSourcesLocation.Location = new System.Drawing.Point(853, 30);
            this.buttonBrowseSourcesLocation.Name = "buttonBrowseSourcesLocation";
            this.buttonBrowseSourcesLocation.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseSourcesLocation.TabIndex = 2;
            this.buttonBrowseSourcesLocation.Text = "Browse...";
            this.buttonBrowseSourcesLocation.UseVisualStyleBackColor = true;
            this.buttonBrowseSourcesLocation.Click += new System.EventHandler(this.buttonBrowseSourcesLocation_Click);
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.folderBrowserDialog.ShowNewFolderButton = false;
            // 
            // tabControlMain
            // 
            this.tabControlMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlMain.Controls.Add(this.tabPageDictionaries);
            this.tabControlMain.Controls.Add(this.tabPageActions);
            this.tabControlMain.Controls.Add(this.tabPageEvents);
            this.tabControlMain.Controls.Add(this.tabPageDocuments);
            this.tabControlMain.Controls.Add(this.tabPageStatistics);
            this.tabControlMain.ImageList = this.imageListTabs;
            this.tabControlMain.ItemSize = new System.Drawing.Size(120, 26);
            this.tabControlMain.Location = new System.Drawing.Point(12, 67);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(984, 669);
            this.tabControlMain.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControlMain.TabIndex = 5;
            // 
            // tabPageDictionaries
            // 
            this.tabPageDictionaries.Controls.Add(this.label4);
            this.tabPageDictionaries.Controls.Add(this.textBoxLabelFilter);
            this.tabPageDictionaries.Controls.Add(this.buttonDictionariesFilter);
            this.tabPageDictionaries.Controls.Add(this.label3);
            this.tabPageDictionaries.Controls.Add(this.textBoxIdFilter);
            this.tabPageDictionaries.Controls.Add(this.label2);
            this.tabPageDictionaries.Controls.Add(this.textBoxProductFilter);
            this.tabPageDictionaries.Controls.Add(this.splitContainerDictionaries);
            this.tabPageDictionaries.ImageIndex = 0;
            this.tabPageDictionaries.Location = new System.Drawing.Point(4, 30);
            this.tabPageDictionaries.Name = "tabPageDictionaries";
            this.tabPageDictionaries.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDictionaries.Size = new System.Drawing.Size(976, 635);
            this.tabPageDictionaries.TabIndex = 0;
            this.tabPageDictionaries.Text = "Dictionaries";
            this.tabPageDictionaries.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(417, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "&Label:";
            // 
            // textBoxLabelFilter
            // 
            this.textBoxLabelFilter.Location = new System.Drawing.Point(459, 6);
            this.textBoxLabelFilter.Name = "textBoxLabelFilter";
            this.textBoxLabelFilter.Size = new System.Drawing.Size(302, 21);
            this.textBoxLabelFilter.TabIndex = 8;
            this.textBoxLabelFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxLabelFilter_KeyDown);
            // 
            // buttonDictionariesFilter
            // 
            this.buttonDictionariesFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDictionariesFilter.Location = new System.Drawing.Point(895, 3);
            this.buttonDictionariesFilter.Name = "buttonDictionariesFilter";
            this.buttonDictionariesFilter.Size = new System.Drawing.Size(75, 24);
            this.buttonDictionariesFilter.TabIndex = 9;
            this.buttonDictionariesFilter.Text = "Refresh";
            this.buttonDictionariesFilter.UseVisualStyleBackColor = true;
            this.buttonDictionariesFilter.Click += new System.EventHandler(this.buttonDictionariesFilter_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(131, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "&ID:";
            // 
            // textBoxIdFilter
            // 
            this.textBoxIdFilter.Location = new System.Drawing.Point(159, 5);
            this.textBoxIdFilter.Name = "textBoxIdFilter";
            this.textBoxIdFilter.Size = new System.Drawing.Size(237, 21);
            this.textBoxIdFilter.TabIndex = 7;
            this.textBoxIdFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxIdFilter_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.CausesValidation = false;
            this.label2.Location = new System.Drawing.Point(6, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "&Product:";
            // 
            // textBoxProductFilter
            // 
            this.textBoxProductFilter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.textBoxProductFilter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.textBoxProductFilter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxProductFilter.Location = new System.Drawing.Point(60, 6);
            this.textBoxProductFilter.Name = "textBoxProductFilter";
            this.textBoxProductFilter.Size = new System.Drawing.Size(51, 21);
            this.textBoxProductFilter.TabIndex = 6;
            this.textBoxProductFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxProductFilter_KeyDown);
            // 
            // splitContainerDictionaries
            // 
            this.splitContainerDictionaries.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerDictionaries.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainerDictionaries.ForeColor = System.Drawing.Color.Transparent;
            this.splitContainerDictionaries.Location = new System.Drawing.Point(0, 33);
            this.splitContainerDictionaries.Name = "splitContainerDictionaries";
            this.splitContainerDictionaries.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerDictionaries.Panel1
            // 
            this.splitContainerDictionaries.Panel1.Controls.Add(this.dataGridViewDictionaries);
            this.splitContainerDictionaries.Panel1.ForeColor = System.Drawing.Color.Transparent;
            // 
            // splitContainerDictionaries.Panel2
            // 
            this.splitContainerDictionaries.Panel2.Controls.Add(this.textBoxDictionaryItem);
            this.splitContainerDictionaries.Size = new System.Drawing.Size(973, 604);
            this.splitContainerDictionaries.SplitterDistance = 447;
            this.splitContainerDictionaries.TabIndex = 10;
            // 
            // dataGridViewDictionaries
            // 
            this.dataGridViewDictionaries.AllowUserToAddRows = false;
            this.dataGridViewDictionaries.AllowUserToDeleteRows = false;
            this.dataGridViewDictionaries.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGridViewDictionaries.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewDictionaries.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDictionaries.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewDictionaries.CausesValidation = false;
            this.dataGridViewDictionaries.ColumnHeadersHeight = 24;
            this.dataGridViewDictionaries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewDictionaries.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDictionaries.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewDictionaries.MultiSelect = false;
            this.dataGridViewDictionaries.Name = "dataGridViewDictionaries";
            this.dataGridViewDictionaries.ReadOnly = true;
            this.dataGridViewDictionaries.RowHeadersVisible = false;
            this.dataGridViewDictionaries.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGridViewDictionaries.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewDictionaries.ShowCellErrors = false;
            this.dataGridViewDictionaries.ShowCellToolTips = false;
            this.dataGridViewDictionaries.ShowEditingIcon = false;
            this.dataGridViewDictionaries.ShowRowErrors = false;
            this.dataGridViewDictionaries.Size = new System.Drawing.Size(973, 447);
            this.dataGridViewDictionaries.TabIndex = 10;
            this.dataGridViewDictionaries.SelectionChanged += new System.EventHandler(this.dataGridViewDictionaries_SelectionChanged);
            // 
            // textBoxDictionaryItem
            // 
            this.textBoxDictionaryItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxDictionaryItem.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDictionaryItem.Location = new System.Drawing.Point(0, 0);
            this.textBoxDictionaryItem.Multiline = true;
            this.textBoxDictionaryItem.Name = "textBoxDictionaryItem";
            this.textBoxDictionaryItem.Size = new System.Drawing.Size(973, 153);
            this.textBoxDictionaryItem.TabIndex = 10;
            // 
            // tabPageActions
            // 
            this.tabPageActions.Controls.Add(this.buttonActionsClearFilter);
            this.tabPageActions.Controls.Add(this.buttonActionsFilter);
            this.tabPageActions.Controls.Add(this.dataTreeListViewActions);
            this.tabPageActions.Controls.Add(this.label9);
            this.tabPageActions.Controls.Add(this.textBox3);
            this.tabPageActions.Controls.Add(this.label10);
            this.tabPageActions.Controls.Add(this.textBoxActionNameFilter);
            this.tabPageActions.ImageIndex = 1;
            this.tabPageActions.Location = new System.Drawing.Point(4, 30);
            this.tabPageActions.Name = "tabPageActions";
            this.tabPageActions.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageActions.Size = new System.Drawing.Size(976, 635);
            this.tabPageActions.TabIndex = 4;
            this.tabPageActions.Text = "Actions";
            this.tabPageActions.UseVisualStyleBackColor = true;
            // 
            // buttonActionsClearFilter
            // 
            this.buttonActionsClearFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonActionsClearFilter.Image = global::Stingray_Developer_Console.Properties.Resources.filter_clear_32;
            this.buttonActionsClearFilter.Location = new System.Drawing.Point(878, 3);
            this.buttonActionsClearFilter.Name = "buttonActionsClearFilter";
            this.buttonActionsClearFilter.Size = new System.Drawing.Size(48, 40);
            this.buttonActionsClearFilter.TabIndex = 39;
            this.buttonActionsClearFilter.UseVisualStyleBackColor = true;
            this.buttonActionsClearFilter.Click += new System.EventHandler(this.buttonActionsClearFilter_Click);
            // 
            // buttonActionsFilter
            // 
            this.buttonActionsFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonActionsFilter.Image = global::Stingray_Developer_Console.Properties.Resources.filter_32;
            this.buttonActionsFilter.Location = new System.Drawing.Point(927, 3);
            this.buttonActionsFilter.Name = "buttonActionsFilter";
            this.buttonActionsFilter.Size = new System.Drawing.Size(48, 40);
            this.buttonActionsFilter.TabIndex = 38;
            this.buttonActionsFilter.UseVisualStyleBackColor = true;
            this.buttonActionsFilter.Click += new System.EventHandler(this.buttonActionsFilter_Click);
            // 
            // dataTreeListViewActions
            // 
            this.dataTreeListViewActions.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.dataTreeListViewActions.AllColumns.Add(this.olvColumnName);
            this.dataTreeListViewActions.AllColumns.Add(this.olvColumnClass);
            this.dataTreeListViewActions.AlternateRowBackColor = System.Drawing.SystemColors.ControlLight;
            this.dataTreeListViewActions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataTreeListViewActions.AutoGenerateColumns = false;
            this.dataTreeListViewActions.CellEditUseWholeCell = false;
            this.dataTreeListViewActions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumnName,
            this.olvColumnClass});
            this.dataTreeListViewActions.ContextMenuStrip = this.contextMenuStripActions;
            this.dataTreeListViewActions.Cursor = System.Windows.Forms.Cursors.Default;
            this.dataTreeListViewActions.DataSource = null;
            this.dataTreeListViewActions.FullRowSelect = true;
            this.dataTreeListViewActions.Location = new System.Drawing.Point(0, 46);
            this.dataTreeListViewActions.MultiSelect = false;
            this.dataTreeListViewActions.Name = "dataTreeListViewActions";
            this.dataTreeListViewActions.ParentKeyAspectName = "";
            this.dataTreeListViewActions.RootKeyValueString = "";
            this.dataTreeListViewActions.RowHeight = 24;
            this.dataTreeListViewActions.ShowCommandMenuOnRightClick = true;
            this.dataTreeListViewActions.ShowGroups = false;
            this.dataTreeListViewActions.Size = new System.Drawing.Size(977, 591);
            this.dataTreeListViewActions.TabIndex = 37;
            this.dataTreeListViewActions.UseCompatibleStateImageBehavior = false;
            this.dataTreeListViewActions.UseHotControls = false;
            this.dataTreeListViewActions.View = System.Windows.Forms.View.Details;
            this.dataTreeListViewActions.VirtualMode = true;
            this.dataTreeListViewActions.ItemActivate += new System.EventHandler(this.dataTreeListViewActions_ItemActivate);
            // 
            // olvColumnName
            // 
            this.olvColumnName.AspectName = "Name";
            this.olvColumnName.AutoCompleteEditor = false;
            this.olvColumnName.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.olvColumnName.Text = "Name";
            this.olvColumnName.Width = 320;
            // 
            // olvColumnClass
            // 
            this.olvColumnClass.AspectName = "Class";
            this.olvColumnClass.AutoCompleteEditor = false;
            this.olvColumnClass.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.olvColumnClass.FillsFreeSpace = true;
            this.olvColumnClass.Text = "Class";
            this.olvColumnClass.Width = 320;
            // 
            // contextMenuStripActions
            // 
            this.contextMenuStripActions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemActionsCopy});
            this.contextMenuStripActions.Name = "contextMenuStripActions";
            this.contextMenuStripActions.Size = new System.Drawing.Size(159, 26);
            // 
            // toolStripMenuItemActionsCopy
            // 
            this.toolStripMenuItemActionsCopy.Font = new System.Drawing.Font("Tahoma", 8F);
            this.toolStripMenuItemActionsCopy.Name = "toolStripMenuItemActionsCopy";
            this.toolStripMenuItemActionsCopy.Size = new System.Drawing.Size(158, 22);
            this.toolStripMenuItemActionsCopy.Text = "Copy to clipboard";
            this.toolStripMenuItemActionsCopy.Click += new System.EventHandler(this.toolStripMenuItemActionsCopy_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(211, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 36;
            this.label9.Text = "&Action:";
            this.label9.Visible = false;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(258, 14);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(165, 21);
            this.textBox3.TabIndex = 34;
            this.textBox3.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.CausesValidation = false;
            this.label10.Location = new System.Drawing.Point(6, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "&Name:";
            // 
            // textBoxActionNameFilter
            // 
            this.textBoxActionNameFilter.Location = new System.Drawing.Point(54, 14);
            this.textBoxActionNameFilter.Name = "textBoxActionNameFilter";
            this.textBoxActionNameFilter.Size = new System.Drawing.Size(142, 21);
            this.textBoxActionNameFilter.TabIndex = 33;
            this.textBoxActionNameFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxActionNameFilter_KeyDown);
            // 
            // tabPageEvents
            // 
            this.tabPageEvents.Controls.Add(this.buttonEventsClearFilter);
            this.tabPageEvents.Controls.Add(this.label5);
            this.tabPageEvents.Controls.Add(this.textBoxEventLabelFilter);
            this.tabPageEvents.Controls.Add(this.buttonEventsFilter);
            this.tabPageEvents.Controls.Add(this.label6);
            this.tabPageEvents.Controls.Add(this.textBoxEventActionFilter);
            this.tabPageEvents.Controls.Add(this.label7);
            this.tabPageEvents.Controls.Add(this.textBoxEventNameFilter);
            this.tabPageEvents.Controls.Add(this.splitContainerEvents);
            this.tabPageEvents.ImageIndex = 2;
            this.tabPageEvents.Location = new System.Drawing.Point(4, 30);
            this.tabPageEvents.Name = "tabPageEvents";
            this.tabPageEvents.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEvents.Size = new System.Drawing.Size(976, 635);
            this.tabPageEvents.TabIndex = 1;
            this.tabPageEvents.Text = "Events";
            this.tabPageEvents.UseVisualStyleBackColor = true;
            // 
            // buttonEventsClearFilter
            // 
            this.buttonEventsClearFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEventsClearFilter.Image = global::Stingray_Developer_Console.Properties.Resources.filter_clear_32;
            this.buttonEventsClearFilter.Location = new System.Drawing.Point(878, 3);
            this.buttonEventsClearFilter.Name = "buttonEventsClearFilter";
            this.buttonEventsClearFilter.Size = new System.Drawing.Size(48, 40);
            this.buttonEventsClearFilter.TabIndex = 32;
            this.buttonEventsClearFilter.UseVisualStyleBackColor = true;
            this.buttonEventsClearFilter.Click += new System.EventHandler(this.buttonEventsClearFilter_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(429, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "&Label:";
            // 
            // textBoxEventLabelFilter
            // 
            this.textBoxEventLabelFilter.Location = new System.Drawing.Point(471, 14);
            this.textBoxEventLabelFilter.Name = "textBoxEventLabelFilter";
            this.textBoxEventLabelFilter.Size = new System.Drawing.Size(204, 21);
            this.textBoxEventLabelFilter.TabIndex = 28;
            this.textBoxEventLabelFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEventLabelFilter_KeyDown);
            // 
            // buttonEventsFilter
            // 
            this.buttonEventsFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEventsFilter.Image = global::Stingray_Developer_Console.Properties.Resources.filter_32;
            this.buttonEventsFilter.Location = new System.Drawing.Point(927, 3);
            this.buttonEventsFilter.Name = "buttonEventsFilter";
            this.buttonEventsFilter.Size = new System.Drawing.Size(48, 40);
            this.buttonEventsFilter.TabIndex = 29;
            this.buttonEventsFilter.UseVisualStyleBackColor = true;
            this.buttonEventsFilter.Click += new System.EventHandler(this.buttonEventsFilter_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(211, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "&Action:";
            // 
            // textBoxEventActionFilter
            // 
            this.textBoxEventActionFilter.Location = new System.Drawing.Point(258, 14);
            this.textBoxEventActionFilter.Name = "textBoxEventActionFilter";
            this.textBoxEventActionFilter.Size = new System.Drawing.Size(165, 21);
            this.textBoxEventActionFilter.TabIndex = 27;
            this.textBoxEventActionFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEventActionFilter_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.CausesValidation = false;
            this.label7.Location = new System.Drawing.Point(6, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "&Name:";
            // 
            // textBoxEventNameFilter
            // 
            this.textBoxEventNameFilter.Location = new System.Drawing.Point(54, 14);
            this.textBoxEventNameFilter.Name = "textBoxEventNameFilter";
            this.textBoxEventNameFilter.Size = new System.Drawing.Size(142, 21);
            this.textBoxEventNameFilter.TabIndex = 26;
            this.textBoxEventNameFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEventNameFilter_KeyDown);
            // 
            // splitContainerEvents
            // 
            this.splitContainerEvents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerEvents.Location = new System.Drawing.Point(0, 46);
            this.splitContainerEvents.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainerEvents.Name = "splitContainerEvents";
            this.splitContainerEvents.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerEvents.Panel1
            // 
            this.splitContainerEvents.Panel1.Controls.Add(this.dataGridViewEvents);
            // 
            // splitContainerEvents.Panel2
            // 
            this.splitContainerEvents.Panel2.Controls.Add(this.tabControlEvent);
            this.splitContainerEvents.Panel2MinSize = 206;
            this.splitContainerEvents.Size = new System.Drawing.Size(974, 592);
            this.splitContainerEvents.SplitterDistance = 386;
            this.splitContainerEvents.TabIndex = 11;
            // 
            // dataGridViewEvents
            // 
            this.dataGridViewEvents.AllowUserToAddRows = false;
            this.dataGridViewEvents.AllowUserToDeleteRows = false;
            this.dataGridViewEvents.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGridViewEvents.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewEvents.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewEvents.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewEvents.ColumnHeadersHeight = 24;
            this.dataGridViewEvents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewEvents.Cursor = System.Windows.Forms.Cursors.Default;
            this.dataGridViewEvents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewEvents.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewEvents.MultiSelect = false;
            this.dataGridViewEvents.Name = "dataGridViewEvents";
            this.dataGridViewEvents.ReadOnly = true;
            this.dataGridViewEvents.RowHeadersVisible = false;
            this.dataGridViewEvents.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewEvents.ShowCellErrors = false;
            this.dataGridViewEvents.ShowCellToolTips = false;
            this.dataGridViewEvents.ShowEditingIcon = false;
            this.dataGridViewEvents.ShowRowErrors = false;
            this.dataGridViewEvents.Size = new System.Drawing.Size(974, 386);
            this.dataGridViewEvents.TabIndex = 9;
            this.dataGridViewEvents.SelectionChanged += new System.EventHandler(this.dataGridViewEvents_SelectionChanged);
            // 
            // tabControlEvent
            // 
            this.tabControlEvent.Controls.Add(this.tabPageEventDetails);
            this.tabControlEvent.Controls.Add(this.tabPageEventXML);
            this.tabControlEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlEvent.ItemSize = new System.Drawing.Size(126, 26);
            this.tabControlEvent.Location = new System.Drawing.Point(0, 0);
            this.tabControlEvent.Name = "tabControlEvent";
            this.tabControlEvent.SelectedIndex = 0;
            this.tabControlEvent.Size = new System.Drawing.Size(974, 206);
            this.tabControlEvent.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControlEvent.TabIndex = 10;
            // 
            // tabPageEventDetails
            // 
            this.tabPageEventDetails.Controls.Add(this.labelEventProductCheck);
            this.tabPageEventDetails.Controls.Add(this.labelEventAgencyCheck);
            this.tabPageEventDetails.Controls.Add(this.labelEventMatchStatus);
            this.tabPageEventDetails.Controls.Add(this.label18);
            this.tabPageEventDetails.Controls.Add(this.textBoxEventProductCheck);
            this.tabPageEventDetails.Controls.Add(this.label17);
            this.tabPageEventDetails.Controls.Add(this.textBoxEventAgencyCheck);
            this.tabPageEventDetails.Controls.Add(this.label16);
            this.tabPageEventDetails.Controls.Add(this.textBoxEventMatchStatus);
            this.tabPageEventDetails.Controls.Add(this.buttonEventActionGoTo);
            this.tabPageEventDetails.Controls.Add(this.label15);
            this.tabPageEventDetails.Controls.Add(this.textBoxEventACL);
            this.tabPageEventDetails.Controls.Add(this.label14);
            this.tabPageEventDetails.Controls.Add(this.textBoxEventPath);
            this.tabPageEventDetails.Controls.Add(this.label13);
            this.tabPageEventDetails.Controls.Add(this.textBoxEventStrategy);
            this.tabPageEventDetails.Controls.Add(this.label12);
            this.tabPageEventDetails.Controls.Add(this.textBoxEventLabel);
            this.tabPageEventDetails.Controls.Add(this.label11);
            this.tabPageEventDetails.Controls.Add(this.textBoxEventAction);
            this.tabPageEventDetails.Controls.Add(this.textBoxEventName);
            this.tabPageEventDetails.Controls.Add(this.label8);
            this.tabPageEventDetails.Location = new System.Drawing.Point(4, 30);
            this.tabPageEventDetails.Name = "tabPageEventDetails";
            this.tabPageEventDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEventDetails.Size = new System.Drawing.Size(966, 172);
            this.tabPageEventDetails.TabIndex = 0;
            this.tabPageEventDetails.Text = "Details";
            this.tabPageEventDetails.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(433, 144);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 13);
            this.label18.TabIndex = 18;
            this.label18.Text = "Product Check";
            // 
            // textBoxEventProductCheck
            // 
            this.textBoxEventProductCheck.Location = new System.Drawing.Point(533, 141);
            this.textBoxEventProductCheck.Name = "textBoxEventProductCheck";
            this.textBoxEventProductCheck.ReadOnly = true;
            this.textBoxEventProductCheck.Size = new System.Drawing.Size(160, 21);
            this.textBoxEventProductCheck.TabIndex = 17;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(433, 117);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 13);
            this.label17.TabIndex = 16;
            this.label17.Text = "Agency Check";
            // 
            // textBoxEventAgencyCheck
            // 
            this.textBoxEventAgencyCheck.Location = new System.Drawing.Point(533, 114);
            this.textBoxEventAgencyCheck.Name = "textBoxEventAgencyCheck";
            this.textBoxEventAgencyCheck.ReadOnly = true;
            this.textBoxEventAgencyCheck.Size = new System.Drawing.Size(160, 21);
            this.textBoxEventAgencyCheck.TabIndex = 15;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(433, 90);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 13);
            this.label16.TabIndex = 14;
            this.label16.Text = "Match Status";
            // 
            // textBoxEventMatchStatus
            // 
            this.textBoxEventMatchStatus.Location = new System.Drawing.Point(533, 87);
            this.textBoxEventMatchStatus.Name = "textBoxEventMatchStatus";
            this.textBoxEventMatchStatus.ReadOnly = true;
            this.textBoxEventMatchStatus.Size = new System.Drawing.Size(160, 21);
            this.textBoxEventMatchStatus.TabIndex = 13;
            // 
            // buttonEventActionGoTo
            // 
            this.buttonEventActionGoTo.Image = global::Stingray_Developer_Console.Properties.Resources.actions_24;
            this.buttonEventActionGoTo.Location = new System.Drawing.Point(250, 30);
            this.buttonEventActionGoTo.Name = "buttonEventActionGoTo";
            this.buttonEventActionGoTo.Size = new System.Drawing.Size(26, 26);
            this.buttonEventActionGoTo.TabIndex = 12;
            this.buttonEventActionGoTo.UseVisualStyleBackColor = true;
            this.buttonEventActionGoTo.Click += new System.EventHandler(this.buttonEventActionGoTo_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 144);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(26, 13);
            this.label15.TabIndex = 11;
            this.label15.Text = "ACL";
            // 
            // textBoxEventACL
            // 
            this.textBoxEventACL.Location = new System.Drawing.Point(84, 141);
            this.textBoxEventACL.Name = "textBoxEventACL";
            this.textBoxEventACL.ReadOnly = true;
            this.textBoxEventACL.Size = new System.Drawing.Size(160, 21);
            this.textBoxEventACL.TabIndex = 10;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 117);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "Path";
            // 
            // textBoxEventPath
            // 
            this.textBoxEventPath.Location = new System.Drawing.Point(84, 114);
            this.textBoxEventPath.Name = "textBoxEventPath";
            this.textBoxEventPath.ReadOnly = true;
            this.textBoxEventPath.Size = new System.Drawing.Size(160, 21);
            this.textBoxEventPath.TabIndex = 8;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 90);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Strategy";
            // 
            // textBoxEventStrategy
            // 
            this.textBoxEventStrategy.Location = new System.Drawing.Point(84, 87);
            this.textBoxEventStrategy.Name = "textBoxEventStrategy";
            this.textBoxEventStrategy.ReadOnly = true;
            this.textBoxEventStrategy.Size = new System.Drawing.Size(160, 21);
            this.textBoxEventStrategy.TabIndex = 6;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 63);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Label";
            // 
            // textBoxEventLabel
            // 
            this.textBoxEventLabel.Location = new System.Drawing.Point(84, 60);
            this.textBoxEventLabel.Name = "textBoxEventLabel";
            this.textBoxEventLabel.ReadOnly = true;
            this.textBoxEventLabel.Size = new System.Drawing.Size(320, 21);
            this.textBoxEventLabel.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Action";
            // 
            // textBoxEventAction
            // 
            this.textBoxEventAction.Location = new System.Drawing.Point(84, 33);
            this.textBoxEventAction.Name = "textBoxEventAction";
            this.textBoxEventAction.ReadOnly = true;
            this.textBoxEventAction.Size = new System.Drawing.Size(160, 21);
            this.textBoxEventAction.TabIndex = 2;
            // 
            // textBoxEventName
            // 
            this.textBoxEventName.Location = new System.Drawing.Point(84, 6);
            this.textBoxEventName.Name = "textBoxEventName";
            this.textBoxEventName.ReadOnly = true;
            this.textBoxEventName.Size = new System.Drawing.Size(160, 21);
            this.textBoxEventName.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Name";
            // 
            // tabPageEventXML
            // 
            this.tabPageEventXML.Controls.Add(this.textBoxEventXML);
            this.tabPageEventXML.Location = new System.Drawing.Point(4, 30);
            this.tabPageEventXML.Name = "tabPageEventXML";
            this.tabPageEventXML.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEventXML.Size = new System.Drawing.Size(966, 172);
            this.tabPageEventXML.TabIndex = 1;
            this.tabPageEventXML.Text = "XML";
            this.tabPageEventXML.UseVisualStyleBackColor = true;
            // 
            // textBoxEventXML
            // 
            this.textBoxEventXML.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBoxEventXML.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxEventXML.Font = new System.Drawing.Font("Consolas", 8F);
            this.textBoxEventXML.Location = new System.Drawing.Point(3, 3);
            this.textBoxEventXML.Multiline = true;
            this.textBoxEventXML.Name = "textBoxEventXML";
            this.textBoxEventXML.Size = new System.Drawing.Size(960, 166);
            this.textBoxEventXML.TabIndex = 10;
            // 
            // tabPageDocuments
            // 
            this.tabPageDocuments.Controls.Add(this.splitContainer2);
            this.tabPageDocuments.Location = new System.Drawing.Point(4, 30);
            this.tabPageDocuments.Name = "tabPageDocuments";
            this.tabPageDocuments.Size = new System.Drawing.Size(976, 635);
            this.tabPageDocuments.TabIndex = 3;
            this.tabPageDocuments.Text = "Documents";
            this.tabPageDocuments.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dataGridViewDocuments);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.textBox1);
            this.splitContainer2.Size = new System.Drawing.Size(976, 635);
            this.splitContainer2.SplitterDistance = 317;
            this.splitContainer2.TabIndex = 12;
            // 
            // dataGridViewDocuments
            // 
            this.dataGridViewDocuments.AllowUserToAddRows = false;
            this.dataGridViewDocuments.AllowUserToDeleteRows = false;
            this.dataGridViewDocuments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDocuments.ColumnHeadersVisible = false;
            this.dataGridViewDocuments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDocuments.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewDocuments.Name = "dataGridViewDocuments";
            this.dataGridViewDocuments.ReadOnly = true;
            this.dataGridViewDocuments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewDocuments.Size = new System.Drawing.Size(976, 317);
            this.dataGridViewDocuments.TabIndex = 9;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(976, 314);
            this.textBox1.TabIndex = 9;
            // 
            // tabPageStatistics
            // 
            this.tabPageStatistics.BackColor = System.Drawing.Color.Transparent;
            this.tabPageStatistics.Controls.Add(this.dataGridViewStatistics);
            this.tabPageStatistics.ImageIndex = 3;
            this.tabPageStatistics.Location = new System.Drawing.Point(4, 30);
            this.tabPageStatistics.Name = "tabPageStatistics";
            this.tabPageStatistics.Size = new System.Drawing.Size(976, 635);
            this.tabPageStatistics.TabIndex = 2;
            this.tabPageStatistics.Text = "Statistics";
            // 
            // dataGridViewStatistics
            // 
            this.dataGridViewStatistics.AllowUserToAddRows = false;
            this.dataGridViewStatistics.AllowUserToDeleteRows = false;
            this.dataGridViewStatistics.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGridViewStatistics.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewStatistics.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewStatistics.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewStatistics.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewStatistics.CausesValidation = false;
            this.dataGridViewStatistics.ColumnHeadersHeight = 28;
            this.dataGridViewStatistics.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewStatistics.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewStatistics.Name = "dataGridViewStatistics";
            this.dataGridViewStatistics.ReadOnly = true;
            this.dataGridViewStatistics.RowHeadersVisible = false;
            this.dataGridViewStatistics.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGridViewStatistics.RowTemplate.ReadOnly = true;
            this.dataGridViewStatistics.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewStatistics.ShowCellErrors = false;
            this.dataGridViewStatistics.ShowCellToolTips = false;
            this.dataGridViewStatistics.ShowEditingIcon = false;
            this.dataGridViewStatistics.ShowRowErrors = false;
            this.dataGridViewStatistics.Size = new System.Drawing.Size(970, 634);
            this.dataGridViewStatistics.TabIndex = 10;
            // 
            // imageListTabs
            // 
            this.imageListTabs.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTabs.ImageStream")));
            this.imageListTabs.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTabs.Images.SetKeyName(0, "dictionary.png");
            this.imageListTabs.Images.SetKeyName(1, "actions.png");
            this.imageListTabs.Images.SetKeyName(2, "events.png");
            this.imageListTabs.Images.SetKeyName(3, "statistics-24.png");
            // 
            // statusStrip
            // 
            this.statusStrip.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel,
            this.toolStripTimingLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 739);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip.TabIndex = 9;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(908, 17);
            this.toolStripStatusLabel.Spring = true;
            // 
            // toolStripTimingLabel
            // 
            this.toolStripTimingLabel.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedInner;
            this.toolStripTimingLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripTimingLabel.Name = "toolStripTimingLabel";
            this.toolStripTimingLabel.Size = new System.Drawing.Size(85, 17);
            this.toolStripTimingLabel.Text = "                          ";
            this.toolStripTimingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonParseSources
            // 
            this.buttonParseSources.Image = global::Stingray_Developer_Console.Properties.Resources.go;
            this.buttonParseSources.Location = new System.Drawing.Point(934, 27);
            this.buttonParseSources.Name = "buttonParseSources";
            this.buttonParseSources.Size = new System.Drawing.Size(62, 48);
            this.buttonParseSources.TabIndex = 3;
            this.buttonParseSources.UseVisualStyleBackColor = true;
            this.buttonParseSources.Click += new System.EventHandler(this.buttonParseSources_Click);
            // 
            // menuStripMain
            // 
            this.menuStripMain.AutoSize = false;
            this.menuStripMain.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuStripItemAbout});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStripMain.Size = new System.Drawing.Size(1008, 24);
            this.menuStripMain.TabIndex = 10;
            this.menuStripMain.Text = "menuStrip";
            // 
            // menuStripItemAbout
            // 
            this.menuStripItemAbout.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.menuStripItemAbout.Name = "menuStripItemAbout";
            this.menuStripItemAbout.Size = new System.Drawing.Size(48, 20);
            this.menuStripItemAbout.Text = "&About";
            this.menuStripItemAbout.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelEventMatchStatus
            // 
            this.labelEventMatchStatus.AutoSize = true;
            this.labelEventMatchStatus.Location = new System.Drawing.Point(699, 90);
            this.labelEventMatchStatus.Name = "labelEventMatchStatus";
            this.labelEventMatchStatus.Size = new System.Drawing.Size(45, 13);
            this.labelEventMatchStatus.TabIndex = 20;
            this.labelEventMatchStatus.Text = "STATUS";
            // 
            // labelEventAgencyCheck
            // 
            this.labelEventAgencyCheck.AutoSize = true;
            this.labelEventAgencyCheck.Location = new System.Drawing.Point(699, 117);
            this.labelEventAgencyCheck.Name = "labelEventAgencyCheck";
            this.labelEventAgencyCheck.Size = new System.Drawing.Size(45, 13);
            this.labelEventAgencyCheck.TabIndex = 21;
            this.labelEventAgencyCheck.Text = "STATUS";
            // 
            // labelEventProductCheck
            // 
            this.labelEventProductCheck.AutoSize = true;
            this.labelEventProductCheck.Location = new System.Drawing.Point(699, 144);
            this.labelEventProductCheck.Name = "labelEventProductCheck";
            this.labelEventProductCheck.Size = new System.Drawing.Size(45, 13);
            this.labelEventProductCheck.TabIndex = 22;
            this.labelEventProductCheck.Text = "STATUS";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 761);
            this.ContextMenuStrip = this.contextMenuStripActions;
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStripMain);
            this.Controls.Add(this.buttonParseSources);
            this.Controls.Add(this.buttonBrowseSourcesLocation);
            this.Controls.Add(this.textBoxSourcesLocation);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControlMain);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripMain;
            this.MinimumSize = new System.Drawing.Size(1024, 800);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stingray Developer Console";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.tabControlMain.ResumeLayout(false);
            this.tabPageDictionaries.ResumeLayout(false);
            this.tabPageDictionaries.PerformLayout();
            this.splitContainerDictionaries.Panel1.ResumeLayout(false);
            this.splitContainerDictionaries.Panel2.ResumeLayout(false);
            this.splitContainerDictionaries.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerDictionaries)).EndInit();
            this.splitContainerDictionaries.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDictionaries)).EndInit();
            this.tabPageActions.ResumeLayout(false);
            this.tabPageActions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataTreeListViewActions)).EndInit();
            this.contextMenuStripActions.ResumeLayout(false);
            this.tabPageEvents.ResumeLayout(false);
            this.tabPageEvents.PerformLayout();
            this.splitContainerEvents.Panel1.ResumeLayout(false);
            this.splitContainerEvents.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerEvents)).EndInit();
            this.splitContainerEvents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEvents)).EndInit();
            this.tabControlEvent.ResumeLayout(false);
            this.tabPageEventDetails.ResumeLayout(false);
            this.tabPageEventDetails.PerformLayout();
            this.tabPageEventXML.ResumeLayout(false);
            this.tabPageEventXML.PerformLayout();
            this.tabPageDocuments.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDocuments)).EndInit();
            this.tabPageStatistics.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStatistics)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxSourcesLocation;
        private System.Windows.Forms.Button buttonBrowseSourcesLocation;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button buttonParseSources;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPageDictionaries;
        private System.Windows.Forms.TabPage tabPageEvents;
        private System.Windows.Forms.SplitContainer splitContainerDictionaries;
        private System.Windows.Forms.ImageList imageListTabs;
        private System.Windows.Forms.SplitContainer splitContainerEvents;
        private System.Windows.Forms.DataGridView dataGridViewEvents;
        private System.Windows.Forms.DataGridView dataGridViewDictionaries;
        private System.Windows.Forms.TextBox textBoxDictionaryItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxProductFilter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxIdFilter;
        private System.Windows.Forms.Button buttonDictionariesFilter;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxLabelFilter;
        private System.Windows.Forms.ToolStripStatusLabel toolStripTimingLabel;
        private System.Windows.Forms.TabPage tabPageStatistics;
        private System.Windows.Forms.DataGridView dataGridViewStatistics;
        private System.Windows.Forms.TabPage tabPageDocuments;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView dataGridViewDocuments;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxEventLabelFilter;
        private System.Windows.Forms.Button buttonEventsFilter;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxEventActionFilter;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxEventNameFilter;
        private System.Windows.Forms.Button buttonEventsClearFilter;
        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem menuStripItemAbout;
        private System.Windows.Forms.TabPage tabPageActions;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxActionNameFilter;
        private BrightIdeasSoftware.DataTreeListView dataTreeListViewActions;
        private BrightIdeasSoftware.OLVColumn olvColumnName;
        private BrightIdeasSoftware.OLVColumn olvColumnClass;
        private System.Windows.Forms.Button buttonActionsFilter;
        private System.Windows.Forms.Button buttonActionsClearFilter;
        private System.Windows.Forms.TabControl tabControlEvent;
        private System.Windows.Forms.TabPage tabPageEventDetails;
        private System.Windows.Forms.TabPage tabPageEventXML;
        private System.Windows.Forms.TextBox textBoxEventName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxEventXML;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxEventAction;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxEventLabel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxEventStrategy;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxEventPath;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxEventACL;
        private System.Windows.Forms.Button buttonEventActionGoTo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxEventMatchStatus;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxEventProductCheck;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxEventAgencyCheck;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripActions;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemActionsCopy;
        private System.Windows.Forms.Label labelEventMatchStatus;
        private System.Windows.Forms.Label labelEventAgencyCheck;
        private System.Windows.Forms.Label labelEventProductCheck;
    }
}

