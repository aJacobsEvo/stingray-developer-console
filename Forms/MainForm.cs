﻿using Stingray_Developer_Console.Classes;
using Stingray_Developer_Console.Classes.data;
using Stingray_Developer_Console.Classes.util;
using Stingray_Developer_Console.Classes.xml;
using Stingray_Developer_Console.xml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Stingray_Developer_Console
{
    public partial class MainForm : Form
    {
        /*
         * Members
         */
        List<Product>                           Products                = new List<Product>(16);
        List<Dictionary>                        Dictionaries            = new List<Dictionary>(32);
        List<Dictionary.Item>                   DictionaryItems         = new List<Dictionary.Item>(1024);
        List<Dictionary.Item>                   DictionaryItemsFiltered;
        List<Event>                             Events                  = new List<Event>(256);
        List<Event>                             EventsFiltered;
        IDictionary<String, Bean>               Beans;
        IDictionary<String, Classes.xml.Action> Actions;
        List<ActionDisplay>                     ActionsFiltered;
        int                                     ActionsFilteredNextId;

        /*
         * Constructor
         */
        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonBrowseSourcesLocation_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                // Display...
                this.textBoxSourcesLocation.Text = folderBrowserDialog.SelectedPath;

                // Store setting...
                Properties.Settings.Default.SourcesLocation = this.textBoxSourcesLocation.Text;
                Properties.Settings.Default.Save();

                // Parse...
                buttonParseSources_Click(this, new EventArgs());
            }
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            // Read settings...
            this.textBoxSourcesLocation.Text = Properties.Settings.Default.SourcesLocation.ToString();
            
            // Tab Control...
            foreach (TabPage tabPage in tabControlMain.TabPages) {
                tabPage.Enter += delegate
                {
                    tabPage.Controls.OfType<Control>().First().Focus();
                };
            }

            // DataGrid(s)...
            dataGridViewDictionaries.DoubleBuffered(true);
            dataGridViewEvents      .DoubleBuffered(true);
            dataGridViewStatistics  .DoubleBuffered(true);

            // Parse...
            if (!String.IsNullOrWhiteSpace(textBoxSourcesLocation.Text))
            {
                buttonParseSources_Click(this, new EventArgs());
            }
        }

        private void buttonParseSources_Click(object sender, EventArgs e)
        {
            // Busy...
            Cursor.Current = Cursors.WaitCursor;

            // StopWatch (start)...
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            // Success flag...
            bool success = true;

            // Check location...
            String sourcesLocation = this.textBoxSourcesLocation.Text;
            if (!Directory.Exists(sourcesLocation))
            {
                MessageBox.Show (String.Format("Location not found.\n\n{0}", sourcesLocation), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                success = false;
            }

            // Dictionaries...
            if (success) {
                parseDictionaries(sourcesLocation);
            }

            // Context...
            if (success) {
                success = parseContext(sourcesLocation);
            }

            // Actions...
            if (success) {
                success = parseActions(sourcesLocation);
            }

            // Events...
            if (success) {
                success = parseEvents(sourcesLocation);
            }

            // Sources...
            if (success)
            {
                success = parseSources(sourcesLocation);
            }

            // Statistics...
            if (success)
            {
                displayStatistics();
            }

            // StopWatch (start)...
            stopWatch.Stop();
            toolStripTimingLabel.Text = stopWatch.Elapsed.ToString("mm\\:ss\\.fff");

            // Ready...
            Cursor.Current = Cursors.Default;
        }

        /**
         * Show event details...
         */
        private void dataGridViewEvents_SelectionChanged(object sender, EventArgs e)
        {
            DataGridView dataGridView = ((DataGridView) sender);
            if (dataGridView.SelectedRows.Count > 0)
            {
                //
                DataGridViewRow dataGridViewRow = dataGridView.SelectedRows[0];
                Event           eventData       = EventsFiltered.ElementAt(dataGridViewRow.Index);

                // Details (panel)
                textBoxEventName.Text         = eventData.Name;
                textBoxEventAction.Text       = eventData.Action;
                buttonEventActionGoTo.Visible = !String.IsNullOrWhiteSpace(eventData.Action);
                textBoxEventLabel.Text        = eventData.Label;
                textBoxEventStrategy.Text     = eventData.Strategy;
                textBoxEventPath.Text         = eventData.Path;
                textBoxEventACL.Text          = eventData.ACL;
                textBoxEventMatchStatus.Text  = eventData.MatchStatus;
                labelEventMatchStatus.Text    = Status.get(eventData.MatchStatus);
                textBoxEventAgencyCheck.Text  = eventData.AgencyCheck;
                labelEventAgencyCheck.Text    = Status.get(eventData.AgencyCheck);
                textBoxEventProductCheck.Text = eventData.ProductCheck;
                labelEventProductCheck.Text    = Status.get(eventData.ProductCheck);

                // XML (text)
                textBoxEventXML.Text          = XmlUtils.ToXmlString(eventData);
            }
        }

        /*
         * 
         */
        private void buttonEventActionGoTo_Click(object sender, EventArgs e)
        {
            // "Actions" tab...
            tabControlMain.SelectedIndex = 1;

            // Filter...
            textBoxActionNameFilter.Text = "\"" + textBoxEventAction.Text + "\"";
            displayActions();
        }

        /**
         * Parse product dictionaries...
         */
        private bool parseDictionaries(String pSourcesLocation)
        {
            // Init..
            String dictionariesPath = Path.Combine(pSourcesLocation, "src", "conf", "dictionary");
            if (!Directory.Exists(dictionariesPath))
            {
                MessageBox.Show ("Location not found.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            
            //
            Products.Clear();
            Dictionaries.Clear();
            DictionaryItems.Clear();

            List<String> productCodes = new List<String>(256);
            foreach (string file in Directory.EnumerateFiles(dictionariesPath, "*.xml", SearchOption.TopDirectoryOnly))
            {
                // Deserialize...
                XmlSerializer serializer = new XmlSerializer(typeof(Dictionary));
                StreamReader  reader     = new StreamReader(file);
                Dictionary    dictionary = (Dictionary) serializer.Deserialize(reader);
                reader.Close();

                // Dictionary...
                Dictionaries.Add(dictionary);

                // Product...
                String productCode = Path.GetFileNameWithoutExtension(file).Replace("-dictionary", "").ToUpper();

                foreach (Dictionary.Item directoryItem in dictionary.Items)
                {
                    directoryItem.Product = productCode;
                }

                //
                productCodes.Add(productCode);

                //
                Product product = new Product(productCode);
                product.Statistics.Questions = dictionary.Items.Count;
                Products.Add(product);

                //
                DictionaryItems.AddRange(dictionary.Items);
            }

            // Autocomplete...
            textBoxProductFilter.AutoCompleteCustomSource.Clear();
            textBoxProductFilter.AutoCompleteCustomSource.AddRange(productCodes.ToArray<String>());

            // Display...
            displayDictionaries();

            // Success!
            return true;
        }
        
        /*
         *
         */
        private void dataGridViewDictionaries_SelectionChanged(object sender, EventArgs e)
        {
            DataGridView dataGridView = (DataGridView) sender;

            textBoxDictionaryItem.Clear();
            foreach (DataGridViewRow dataGridViewRow in dataGridView.SelectedRows)
            {
                textBoxDictionaryItem.AppendText(XmlUtils.ToXmlString(DictionaryItemsFiltered.ElementAt(dataGridViewRow.Index)));
            }
        }
        
        /*
         *
         */
        private void displayDictionaries()
        {
            /*
             * Filter...
             */
            IEnumerable<Dictionary.Item> filter = (DictionaryItems != null && DictionaryItems.Any() ? DictionaryItems as IEnumerable<Dictionary.Item>
                                                                                                    : new List<Dictionary.Item>());
            if (!String.IsNullOrWhiteSpace(textBoxProductFilter.Text))
            {
                String productFilter = textBoxProductFilter.Text;
                filter = filter.Where(p => p.Product.Contains(productFilter));
            }
            if (!String.IsNullOrWhiteSpace(textBoxIdFilter.Text))
            {
                String idFilter = textBoxIdFilter.Text.ToLower();
                filter = filter.Where(p => p.ID.ToLower().Contains(idFilter));
            }
            if (!String.IsNullOrWhiteSpace(textBoxLabelFilter.Text))
            {
                String labelFilter = textBoxLabelFilter.Text.ToLower();
                filter = filter.Where(p => p.Label != null && p.Label.ToLower().Contains(labelFilter));
            }
            DictionaryItemsFiltered = filter.ToList();

            /*
             * DataGridView...
             */
            dataGridViewDictionaries.SuspendLayout();
            dataGridViewDictionaries.AutoGenerateColumns = false;
            dataGridViewDictionaries.DataSource          = new SortableBindingList<Dictionary.Item>(DictionaryItemsFiltered);

            if (dataGridViewDictionaries.Columns.Count == 0)
            {
                DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                column.Name             = "Product";
                column.DataPropertyName = "Product";
                column.Width            = 80;
                dataGridViewDictionaries.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name             = "ID";
                column.DataPropertyName = "Id";
                column.MinimumWidth     = 200;
                dataGridViewDictionaries.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name             = "Label";
                column.DataPropertyName = "Label";
                column.AutoSizeMode     = DataGridViewAutoSizeColumnMode.Fill;
                dataGridViewDictionaries.Columns.Add(column);
            }

            dataGridViewDictionaries.ResumeLayout();
        }

        /*
         *
         */
        private void buttonDictionariesFilter_Click(object sender, EventArgs e)
        {
            // Busy...
            Cursor.Current = Cursors.WaitCursor;

            // Work...
            displayDictionaries();

            // Ready...
            Cursor.Current = Cursors.Default;
        }

        /*
         *
         */
        private bool parseContext(String pSourcesLocation)
        {
            // Init...
            String contextLocation  = Path.Combine(pSourcesLocation, "src", "resources", "context.xml");
            if (!File.Exists(contextLocation)) {
                MessageBox.Show ("File not found:\n" + contextLocation, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            // Deserialize...
            using (StreamReader reader = new StreamReader(contextLocation))
            { 
                XmlSerializer serializer = new XmlSerializer(typeof(Beans));
                Beans = ((Beans) serializer.Deserialize(reader)).List.ToDictionary(bean => bean.ID, bean => bean);
            }

            // Success...
            return true;
        }

        /*
         *
         */
        private bool parseActions(String pSourcesLocation)
        {
            // Init...
            String actionsLocation  = Path.Combine(pSourcesLocation, "src", "conf", "common", "action.xml");
            if (!File.Exists(actionsLocation)) {
                MessageBox.Show ("File not found:\n" + actionsLocation, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            // Deserialize...
            XmlSerializer serializer = new XmlSerializer(typeof(Actions));

            using (StreamReader reader = new StreamReader(actionsLocation))
            { 
                Actions = ((Actions) serializer.Deserialize(reader)).List.ToDictionary(action => action.Name, action => action);
            }

            // Filter...
            ActionsFiltered       = new List<ActionDisplay>();
            ActionsFilteredNextId = 1;
            foreach (KeyValuePair<string, Classes.xml.Action> entry in Actions.OrderBy(entry => entry.Key))
            {
                // Process element...
                parseAction(ActionsFilteredNextId++, 0, entry.Value);
            }

            // Display...
            displayActions();

            // Success...
            return true;
        }

        private void parseAction(int id, int parentId, Classes.xml.Action action)
        {
            ActionDisplay actionDisplay = new ActionDisplay();
            actionDisplay.Id       = id;
            actionDisplay.ParentId = parentId;
            actionDisplay.Name     = action.Name;
            actionDisplay.Class    = action.Class;
            ActionsFiltered.Add(actionDisplay);

            // Action - "bean"...
            if (action.Bean)
            {
                Classes.xml.Bean dependsOnBean;
                if (Beans.TryGetValue(action.Name, out dependsOnBean))
                {
                    actionDisplay.Class    = dependsOnBean.Class;
                }
                // Not found...
                else
                {
                    actionDisplay.Class    = "ERROR: NOT FOUND";
                }
            }
            // Action with "depends"...
            else if (!String.IsNullOrEmpty(action.Depends)) {
                List<string> dependsList = action.Depends.Split(',').ToList<string>();

                foreach (string dependsOn in dependsList) {
                    Classes.xml.Action dependsOnAction;
                    if (Actions.TryGetValue(dependsOn, out dependsOnAction))
                    {
                        parseAction(ActionsFilteredNextId++, id, dependsOnAction);
                    }
                    // Not found...
                    else
                    {
                        actionDisplay = new ActionDisplay();
                        actionDisplay.Id       = ActionsFilteredNextId++;
                        actionDisplay.ParentId = id;
                        actionDisplay.Name     = action.Name;
                        actionDisplay.Class    = "ERROR: NOT FOUND";
                        ActionsFiltered.Add(actionDisplay);
                    }
                }
            }
        }

        private void textBoxActionNameFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                displayActions();
            }
        }

        /*
         *
         */
        private void buttonActionsFilter_Click(object sender, EventArgs e)
        {
            // Busy...
            Cursor.Current = Cursors.WaitCursor;

            // Work...
            displayActions();

            // Ready...
            Cursor.Current = Cursors.Default;
        }


        /*
         * 
         */
        private void buttonActionsClearFilter_Click(object sender, EventArgs e)
        {
            // Busy...
            Cursor.Current = Cursors.WaitCursor;

            //
            textBoxActionNameFilter.Clear();

            // Work...
            displayActions();

            // Ready...
            Cursor.Current = Cursors.Default;
        }

        /*
         *  Double click / ENTER handler...
         */
        private void dataTreeListViewActions_ItemActivate(object sender, EventArgs e)
        {
            dataTreeListViewActions.ToggleExpansion(dataTreeListViewActions.GetItem(dataTreeListViewActions.SelectedIndex).RowObject);
        }

        /*
         * Actions - "Copy to clipboard" (context)
         */
        private void toolStripMenuItemActionsCopy_Click(object sender, EventArgs e)
        {
            StringBuilder stringBuilder = new StringBuilder(1024);
            foreach (int index in dataTreeListViewActions.SelectedIndices)
            {
                ActionDisplay actionDisplay = (ActionDisplay) dataTreeListViewActions.GetItem(index).RowObject;
                appendActionDisplayRecursive(actionDisplay, stringBuilder, 0);
            }
            Clipboard.SetText(stringBuilder.ToString());
        }

        /*
         *
         */
        private void appendActionDisplayRecursive(ActionDisplay actionDisplay, StringBuilder stringBuilder, int indentLevel)
        {
            // Current...
            StringBuilder stringBuilderLine = new StringBuilder(128);
            for (int i = 0, n = indentLevel; i < n; i++)
            {
                stringBuilderLine.Append("  ");
            }
            stringBuilderLine.Append("+ ").Append(actionDisplay.Name);
            stringBuilderLine.Append("".PadRight(64 - stringBuilderLine.Length));
            stringBuilderLine.AppendLine(actionDisplay.Class);
            stringBuilder.Append(stringBuilderLine.ToString());

            // Children...
            var actionDisplayChildren = ActionsFiltered.Where(ad => ad.ParentId == actionDisplay.Id).ToList();
            foreach (ActionDisplay actionDisplayChild in actionDisplayChildren) {
                appendActionDisplayRecursive(actionDisplayChild, stringBuilder, indentLevel + 1);
            }
        }

        /*
         * 
         */
        private void displayActions()
        {
            dataTreeListViewActions.SuspendLayout();

            // Expand (slow - not by default)...
            bool expandAll = false;

            //
            dataTreeListViewActions.KeyAspectName = "Id";
            dataTreeListViewActions.ParentKeyAspectName = "ParentId";
            dataTreeListViewActions.RootKeyValue = 0;

            // Filter...
            IEnumerable<ActionDisplay> filter = (ActionsFiltered != null && ActionsFiltered.Any() ? ActionsFiltered as IEnumerable<ActionDisplay>
                                                                                                  : new List<ActionDisplay>());
            if (!String.IsNullOrWhiteSpace(textBoxActionNameFilter.Text))
            {
                String actionNameFilter = textBoxActionNameFilter.Text.ToLower();
                // Containts (*)...
                if (actionNameFilter.Contains("*")) {
                    Regex regex = new Regex("^" + actionNameFilter.Replace(".", "\\.").Replace("*", ".*") + "$");
                    filter = filter.Where(a => (a.ParentId == 0 && regex.IsMatch(a.Name.ToLower())) || a.ParentId > 0);
                }
                // Exact (")...
                else if (actionNameFilter.StartsWith("\""))
                {
                    actionNameFilter = actionNameFilter.Trim('"');
                    filter = filter.Where(a => (a.ParentId == 0 && a.Name.ToLower().Equals(actionNameFilter)) || a.ParentId > 0);
                }
                // Begins...
                else
                {
                    filter = filter.Where(a => (a.ParentId == 0 && a.Name.ToLower().StartsWith(actionNameFilter)) || a.ParentId > 0);
                }

                // Expand...
                expandAll = true;
            }

            // Data...
            dataTreeListViewActions.DataSource = new SortableBindingList<ActionDisplay>(filter);
            if (expandAll) {
                dataTreeListViewActions.ExpandAll();
            }

            dataTreeListViewActions.ResumeLayout();
        }

        /*
         *
         */
        private bool parseEvents(String pSourcesLocation)
        {
            // Init...
            String eventsLocation  = Path.Combine(pSourcesLocation, "src", "conf", "common", "event.xml");
            if (!File.Exists(eventsLocation)) {
                MessageBox.Show ("File not found.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            // Deserialize...
            XmlSerializer serializer = new XmlSerializer(typeof(Events));

            StreamReader reader = new StreamReader(eventsLocation);
            Events = ((Events) serializer.Deserialize(reader)).List;
            reader.Close();

            // Display...
            displayEvents();

            // Success...
            return true;
        }

        /*
         * 
         */
        private void displayEvents()
        {
            /*
             * Filter...
             */
            IEnumerable<Event> filter = (Events != null && Events.Any() ? Events as IEnumerable<Event>
                                                                        : new List<Event>());
            if (!String.IsNullOrWhiteSpace(textBoxEventNameFilter.Text))
            {
                String eventNameFilter = textBoxEventNameFilter.Text.ToLower();                
                if (eventNameFilter.Contains("*")) {
                    Regex regex = new Regex("^" + eventNameFilter.Replace(".", "\\.").Replace("*", ".*") + "$");
                    filter = filter.Where(p => p.Name != null && regex.IsMatch(p.Name.ToLower()));
                }
                else {
                    filter = filter.Where(p => p.Name.ToLower().StartsWith(eventNameFilter));
                }

            }
            if (!String.IsNullOrWhiteSpace(textBoxEventActionFilter.Text))
            {
                String eventActionFilter = textBoxEventActionFilter.Text.ToLower();
                if (eventActionFilter.Contains("*")) {
                    Regex regex = new Regex("^" + eventActionFilter.Replace(".", "\\.").Replace("*", ".*") + "$");
                    filter = filter.Where(p => p.Action != null && regex.IsMatch(p.Action.ToLower()));
                }
                else {
                    filter = filter.Where(p => p.Action != null && p.Action.ToLower().StartsWith(eventActionFilter));
                }
            }
            if (!String.IsNullOrWhiteSpace(textBoxEventLabelFilter.Text))
            {
                String eventLabelFilter = textBoxEventLabelFilter.Text.ToLower();
                filter = filter.Where(p => p.Label  != null && p.Label.ToLower().Contains(eventLabelFilter));
            }
            EventsFiltered = filter.ToList();

            /*
             * DataGridView...
             */
            dataGridViewDictionaries.SuspendLayout();
            dataGridViewEvents.AutoGenerateColumns = false;
            dataGridViewEvents.DataSource          = new SortableBindingList<Event>(EventsFiltered);

            if (dataGridViewEvents.Columns.Count == 0)
            {
                DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                column.Name             = "Name";
                column.DataPropertyName = "Name";
                column.Width            = 200;
                dataGridViewEvents.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name             = "Action";
                column.DataPropertyName = "Action";
                column.MinimumWidth     = 200;
                dataGridViewEvents.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name             = "Label";
                column.DataPropertyName = "Label";
                column.MinimumWidth     = 200;
                column.AutoSizeMode     = DataGridViewAutoSizeColumnMode.Fill;
                dataGridViewEvents.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name             = "Match Status";
                column.DataPropertyName = "matchStatus";
                column.MinimumWidth     = 100;
                dataGridViewEvents.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name             = "Agency Check";
                column.DataPropertyName = "agencyCheck";
                column.MinimumWidth     = 100;
                dataGridViewEvents.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name             = "Product Check";
                column.DataPropertyName = "productCheck";
                column.MinimumWidth     = 100;
                dataGridViewEvents.Columns.Add(column);
            }

            
            dataGridViewEvents.ResumeLayout();
        }

        /*
         * 
         */
        private void textBoxEventNameFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                displayEvents();
            }
        }

        /*
         * 
         */
        private void textBoxEventActionFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                displayEvents();
            }
        }

        /*
         * 
         */
        private void textBoxEventLabelFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                displayEvents();
            }
        }
        

        /*
         * 
         */
        private void buttonEventsClearFilter_Click(object sender, EventArgs e)
        {
            // Busy...
            Cursor.Current = Cursors.WaitCursor;

            //
            textBoxEventNameFilter.Clear();
            textBoxEventActionFilter.Clear();
            textBoxEventLabelFilter.Clear();

            // Work...
            displayEvents();

            // Ready...
            Cursor.Current = Cursors.Default;
        }

        /*
         * 
         */
        private void buttonEventsFilter_Click(object sender, EventArgs e)
        {
            // Busy...
            Cursor.Current = Cursors.WaitCursor;

            // Work...
            displayEvents();

            // Ready...
            Cursor.Current = Cursors.Default;
        }

        /*
         * 
         */
        private bool parseSources(String pSourcesLocation)
        {
            // Init...
            foreach (Product product in Products)
            {
                /*
                 * src/conf/product/XYZ
                 */
                parseSources(Path.Combine(pSourcesLocation, "src", "conf", "product"), product);

                /*
                 * src/java/stingray/products/XYZ
                 */
                parseSources(Path.Combine(pSourcesLocation, "src", "java", "stingray", "products"), product);

                /*
                 * src/java/stingray/validation/XYZ
                 */
                parseSources(Path.Combine(pSourcesLocation, "src", "java", "stingray", "validation"), product);

                /*
                 * src/java/stingray/products/doc/pen/XYZ
                 */
                parseSources(Path.Combine(pSourcesLocation, "src", "java", "stingray", "products", "doc", "pen"), product);
            }

            // Success...
            return true;
        }

        /**
         * Parse product sources...
         */
        private void parseSources(String pSourcesLocation, Product pProduct)
        {
            String sourcesLocation = Path.Combine(pSourcesLocation, pProduct.Code);
            if (Directory.Exists(sourcesLocation))
            {
                foreach (string file in Directory.EnumerateFiles(sourcesLocation, "*", SearchOption.AllDirectories))
                {
                    // Extension...
                    string extension = Path.GetExtension(file);
                    int    lines     = File.ReadLines(file).Count(line => !string.IsNullOrWhiteSpace(line));

                    // Java
                    if (Statistics.EXTENSIONS_JAVA.Contains(extension))
                    {
                        pProduct.Statistics.JavaFiles ++;
                        pProduct.Statistics.JavaLines += lines;
                    }
                    // JavaScript
                    else if (Statistics.EXTENSIONS_JAVASCRIPT.Contains(extension))
                    {
                        pProduct.Statistics.JavaScriptFiles ++;
                        pProduct.Statistics.JavaScriptLines += lines;
                    }
                    // XML/XSD
                    else if (Statistics.EXTENSIONS_XML.Contains(extension))
                    {
                        pProduct.Statistics.XmlFiles ++;
                        pProduct.Statistics.XmlLines += lines;
                    }
                    // Others...
                    else {
                        pProduct.Statistics.OtherFiles ++;
                        pProduct.Statistics.OtherLines += lines;
                    }

                    // Total...
                    pProduct.Statistics.TotalFiles ++;
                    pProduct.Statistics.TotalLines += lines;
                }
            }
        }

        /*
         *
         */
        private void textBoxLabelFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                displayDictionaries();
            }
        }
        
        /*
         *
         */
        private void textBoxProductFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                displayDictionaries();
            }
        }

        /*
         * 
         */
        private void textBoxIdFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                displayDictionaries();
            }
        }

        /**
         * Display statistics...
         */
        private void displayStatistics()
        {
            /*
             * Data...
             */
            List<Statistics> statistics = new List<Statistics>(Products.Count);
            foreach (Product product in Products)
            {
                statistics.Add(product.Statistics);
            }

            /*
             * DataGridView...
             */
            dataGridViewStatistics.SuspendLayout();
            dataGridViewStatistics.AutoGenerateColumns = false;
            dataGridViewStatistics.DataSource          = new SortableBindingList<Statistics>(statistics);

            if (dataGridViewStatistics.Columns.Count == 0)
            {
                DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                column.Name                       = "Product";
                column.DataPropertyName           = "Product";
                column.Width                      = 80;
                dataGridViewStatistics.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name                       = "Questions";
                column.DataPropertyName           = "Questions";
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                column.Width                      = 80;
                dataGridViewStatistics.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name                       = "FILES";
                column.DataPropertyName           = "TotalFiles";
                column.Width                      = 70;
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                column.DefaultCellStyle.Font      = new Font(dataGridViewStatistics.DefaultCellStyle.Font, FontStyle.Bold);
                dataGridViewStatistics.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name                       = "Java";
                column.DataPropertyName           = "JavaFiles";
                column.Width                      = 70;
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridViewStatistics.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name                       = "JavaScript";
                column.DataPropertyName           = "JavaScriptFiles";
                column.Width                      = 70;
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridViewStatistics.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name                       = "XML";
                column.DataPropertyName           = "XmlFiles";
                column.Width                      = 70;
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridViewStatistics.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name                       = "Other";
                column.DataPropertyName           = "OtherFiles";
                column.Width                      = 70;
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridViewStatistics.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name                       = "LINES";
                column.DataPropertyName           = "TotalLines";
                column.Width                      = 70;
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                column.DefaultCellStyle.Font      = new Font(dataGridViewStatistics.DefaultCellStyle.Font, FontStyle.Bold);
                dataGridViewStatistics.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name                       = "Java";
                column.DataPropertyName           = "JavaLines";
                column.Width                      = 70;
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridViewStatistics.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name                       = "JavaScript";
                column.DataPropertyName           = "JavaScriptLines";
                column.Width                      = 70;
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridViewStatistics.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name                       = "XML";
                column.DataPropertyName           = "XmlLines";
                column.Width                      = 70;
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridViewStatistics.Columns.Add(column);

                column = new DataGridViewTextBoxColumn();
                column.Name                       = "Other";
                column.DataPropertyName           = "OtherLines";
                column.Width                      = 70;
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridViewStatistics.Columns.Add(column);
            }

            dataGridViewStatistics.ResumeLayout();
        }
    }
}
