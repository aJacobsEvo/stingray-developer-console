﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stingray_Developer_Console.Classes
{
    class Product
    {
        public Product(String code)
        {
            Code               = code;
            Statistics.Product = Code;
        }

        public String     Code       { get; }
        public Statistics Statistics { get; }  = new Statistics();

    }

    class Statistics
    {
        public static readonly string[] EXTENSIONS_JAVA       = new String[] { ".java" };
        public static readonly string[] EXTENSIONS_JAVASCRIPT = new String[] { ".js"   };
        public static readonly string[] EXTENSIONS_XML        = new String[] { ".xml", ".xsd" };

        public String Product         { get; set; }
        public int    JavaLines       { get; set; } = 0;
        public int    JavaFiles       { get; set; } = 0;
        public int    JavaScriptLines { get; set; } = 0;
        public int    JavaScriptFiles { get; set; } = 0;
        public int    XmlLines        { get; set; } = 0;
        public int    XmlFiles        { get; set; } = 0;
        public int    OtherLines      { get; set; } = 0;
        public int    OtherFiles      { get; set; } = 0;
        public int    TotalFiles      { get; set; } = 0;
        public int    TotalLines      { get; set; } = 0;
        public int    Questions       { get; set; } = 0;
    }
}
