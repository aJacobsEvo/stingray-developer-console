﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stingray_Developer_Console.Classes.data
{
    public class Status
    {
        private static Dictionary<String, String> Dictionary = new Dictionary<string, string>()
        {
            { "acc", "Accepted"              },
            { "new", "Draft"                 },
            { "nsm", "Submitted"             },
            { "nqt", "Quoted"                },
            { "ndc", "Declined"              },
            { "noc", "On Cover"              },
            { "nrj", "NTU"                   },
            { "nlp", "Lapsed"                },
            { "nds", "Discarded"             },
            { "niq", "Indicated"             },
            { "nia", "Indication Accepted"   },
            { "amd", "Draft"                 },
            { "asm", "MTA Submitted"         },
            { "aqt", "MTA Quoted"            },
            { "adc", "MTA Declined"          },
            { "aoc", "MTA On Cover"          },
            { "arj", "MTA NTU"               },
            { "alp", "MTA Lapsed"            },
            { "ads", "MTA Discarded"         },
            { "ren", "Renewal Draft"         },
            { "rsm", "Renewal Submitted"     },
            { "rqt", "Renewal Quoted"        },
            { "rdc", "Renewal Declined"      },
            { "roc", "Renewal On Cover"      },
            { "rrj", "Renewal NTU"           },
            { "rlp", "Renewal Lapsed"        },
            { "can", "Cancelled"             },
            { "psp", "Prospect"              },
            { "psn", "Prospect Not Obtained" },
            { "qtd", "Discarded Quote"       },
            { "dlt", "Deleted"               }
        };

        public static string get(string codes)
        {
            string desc = "";
            if (!String.IsNullOrWhiteSpace(codes))
            {
                string[]     codesArray = codes.Split(' ');
                List<string> descList = new List<string>(codesArray.Length);
                foreach (String code in codesArray)
                {
                    if (!Dictionary.TryGetValue(code, out desc)) {
                        desc = "???";
                    }
                    descList.Add(desc);
                }
                desc = String.Join(", ", descList);
            }
            return desc;
        }
    }
}
