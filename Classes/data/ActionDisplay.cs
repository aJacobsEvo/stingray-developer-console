﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stingray_Developer_Console.Classes.data
{
    class ActionDisplay
    {
        public int    Id       { get; set; }
        public int    ParentId { get; set; }
        public string Name     { get; set; }
        public string Class    { get; set; }
    }
}
