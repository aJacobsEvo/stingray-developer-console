﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Stingray_Developer_Console.Classes.xml
{
    [Serializable()]
    [XmlRoot("items", Namespace = "")]
    public class Dictionary
    {
        [XmlElement("item", typeof(Item))]
        public List<Item> Items { get; set; }

        public Dictionary()
        {
            Items = new List<Item>();
        }

        /*
         *
         */
        [Serializable()]
        [XmlRoot("item")]
        public class Item
        {
            [XmlIgnore]
            public String Product { get; set; }

            [XmlAttribute("id")]
            public String ID { get; set; }

            [XmlAttribute("type")]
            public String Type { get; set; }

            [XmlAttribute("length")]
            public String Length { get; set; }

            [XmlAttribute("labelStyle")]
            public String LabelStyle { get; set; }

            [XmlAttribute("validators")]
            public String Validators { get; set; }

            [XmlElement("label")]
            public String Label { get; set; }
            /*
            [XmlElement("value", typeof(Value))]
            public List<Item> Values { get; set; }

            public class Value
            {
                [XmlAttribute("validators")]
                public String Show { get; set; }

                [XmlAttribute("validators")]
                public String Hide { get; set; }

            }*/
        }
    }
}
