﻿using System;
using System.Xml.Serialization;

namespace Stingray_Developer_Console.Classes.xml
{
    [Serializable()]
    [XmlRoot("action")]
    public class Action
    {
        [XmlAttribute("name")]
        public string Name  { get; set; }

        [XmlAttribute("bean")]
        public bool   Bean  { get; set; }

        [XmlAttribute("class")]
        public string Class { get; set; }

        [XmlAttribute("depends")]
        public string Depends { get; set; }
    }
}
