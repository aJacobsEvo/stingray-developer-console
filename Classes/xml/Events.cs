﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Stingray_Developer_Console.xml
{
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("events")]
    public class Events
    {
        [XmlElement("event", typeof(Event))]
        public List<Event> List { get; set; }

        public Events()
        {
            List = new List<Event>();
        }
    }
}
