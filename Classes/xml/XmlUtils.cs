﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Stingray_Developer_Console.Classes.xml
{
    public static class XmlUtils
    {
        public static string ToXmlString<T>(this T input)
        {
            var xmlSerializer = new XmlSerializer(input.GetType());

            using (var ms = new MemoryStream())
            {
                using (var xw = XmlWriter.Create(ms,
                    new XmlWriterSettings()
                    {
                        Encoding = new UTF8Encoding(false),
                        Indent = true,
                        NewLineOnAttributes = true,
                    }))
                {
                    xmlSerializer.Serialize(xw, input);
                    return Encoding.UTF8.GetString(ms.ToArray());
                }
            }
        }

        public static void ToXml<T>(this T objectToSerialize, Stream stream)
        {
            new XmlSerializer(typeof(T)).Serialize(stream, objectToSerialize);
        }

        public static void ToXml<T>(this T objectToSerialize, StringWriter writer)
        {
            new XmlSerializer(typeof(T)).Serialize(writer, objectToSerialize);
        }
    }
}