﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Stingray_Developer_Console.Classes.xml
{
    [Serializable()]
    [XmlRoot("beans")]
    public class Beans
    {
        [XmlElement("bean", typeof(Bean))]
        public List<Bean> List { get; set; }

        public Beans()
        {
            List = new List<Bean>();
        }
    }
}
