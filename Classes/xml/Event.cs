﻿using System;
using System.Xml.Serialization;

namespace Stingray_Developer_Console.xml
{
    [Serializable()]
    [XmlRoot("event")]
    public class Event
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("action")]
        public string Action { get; set; }

        // NOT SHOWN YET
        // [XmlAttribute("accesskey")]
        // public string AccessKey { get; set; }

        // NOT USED YET
        // [XmlAttribute("anchor")]
        // public string Anchor { get; set; }

        [XmlAttribute("label")]
        public string Label { get; set; }

        [XmlAttribute("strategy")]
        public string Strategy { get; set; }

        [XmlAttribute("path")]
        public string Path { get; set; }

        [XmlAttribute("acl")]
        public string ACL { get; set; }

        [XmlAttribute("ignoreStatus")]
        public string IgnoreStatus { get; set; }

        [XmlAttribute("matchStatus")]
        public string MatchStatus { get; set; }

        [XmlAttribute("agencyCheck")]
        public string AgencyCheck { get; set; }

        [XmlAttribute("productCheck")]
        public string ProductCheck { get; set; }
    }
}
