﻿using System;
using System.Xml.Serialization;

namespace Stingray_Developer_Console.Classes.xml
{
    [Serializable()]
    [XmlRoot("bean")]
    public class Bean
    {
        [XmlAttribute("id")]
        public string ID { get; set; }

        [XmlAttribute("class")]
        public string Class { get; set; }
    }
}
