﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Stingray_Developer_Console.xml
{
    [System.Serializable()]
    [XmlRoot("actions")]
    public class Actions
    {
        [XmlElement("action", typeof(Classes.xml.Action))]
        public List<Classes.xml.Action> List { get; set; }

        public Actions()
        {
            List = new List<Classes.xml.Action>();
        }
    }
}
